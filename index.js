'use strict'

const app = require('express')()
const cors = require('cors')
const helmet = require('helmet')
const morgan = require('morgan')
const healthcheck = require('express-healthcheck')

const ENV = process.env.NODE_ENV || 'development'
const PORT = process.env.PORT || 3000

const handler = (status, message) =>
  (req, res) => res.status(status).json({ message })

app.use(helmet())
app.use(cors())
app.use(morgan('combined'))

app.get('/', handler(200, 'Hello world!'))

app.use('/healthcheck', healthcheck())

app.use('/*', handler(404, 'Route not found!'))

app.listen(PORT,
  () => console.log(`Server up on port ${PORT} for ${ENV} environment`))