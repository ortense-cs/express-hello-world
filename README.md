# HELLO WORLD

A "hello world API" with health check 

## Routes

`GET /`

**Response**

```json
{ "message": "Hello world!" }
``` 

`GET /healthcheck`

**Response**

```json
{ "uptime": 000.000 }
``` 

## Environment variables

`PORT` : Set HTTP port

`NODE_ENV` : Sent environment name

## Commands

- `npm start` To start server
- `npm test` Run test simulation
